=======================================
SKA Logging Configuration Documentation
=======================================

Description
-----------

This project allows standard logging configuration across all SKA projects. The format used is described in detail
on the `developer portal`_. This library should be used to configure the application's logging system as early as
possible at start up. Note that for Python TANGO devices that inherit from `SKABaseDevice`_ this is already done
in the base class, so it does not need to be done again.

.. toctree::
  :maxdepth: 1

   Developer Guide<package/guide>
   API<api/configuration>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _developer portal: https://developer.skatelescope.org/en/latest/development/logging-format.html
.. _SKABaseDevice: https://gitlab.com/ska-telescope/ska-tango-base/-/blob/0.19.1/src/ska_tango_base/base/base_device.py?ref_type=tags#L29

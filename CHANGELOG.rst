###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## unreleased
*************
* Pin poetry version in docs build to 1.7.1

v0.4.3
******
* Upgrade dependencies, and allow `python >3.10,<4.0`

v0.4.2
******
* Use SKA template repository for CI
* Use yaml configuration file for readthedocs
* Use ska-ser-sphinx-theme for documentation

v0.4.1
******
No change, moving artefacts to a new repository https://artefact.skao.int/.

v0.4.0
******
Change packaging to use single namespace.
Usage like `from ska.logging import configure_logging` changes
to `from ska_ser_logging import configure_logging`.

v0.3.0
******
Change packaging to use "ska" namespace.
Usage like `from ska_logging import configure_logging` changes
to `from ska.logging import configure_logging`.

v0.2.1
******
Fix default formatter time.
Resulting output is now `1|2020-01-13T10:42:42.415Z|...` instead `1|2020-01-13 10:42:42,415.415Z|...`

v0.2.0
******
Provide access to default formatter.
The API is available via this public function `get_default_formatter`

v0.1.0
******
* First release of the Python package.
